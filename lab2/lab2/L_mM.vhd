----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:23:30 12/18/2017 
-- Design Name: 
-- Module Name:    L_mM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity L_mM is
    Port ( L_A : in  STD_LOGIC_VECTOR (7 downto 0);
           L_B : in  STD_LOGIC_VECTOR (7 downto 0);
           L_p : out  STD_LOGIC_VECTOR (15 downto 0));
end L_mM;

architecture Behavioral of L_mM is
constant WIDTH: integer:=8;
signal l_ua, l_bv0, l_bv1, l_bv2, l_bv3, l_bv4,l_bv5, l_bv6, l_bv7 : unsigned (WIDTH - 1 downto 0);
signal l_p0, l_p1, l_p2,l_p3, l_p4, l_p5,l_p6, l_p7: unsigned (WIDTH downto 0);
signal l_product: unsigned (2*WIDTH - 1 downto 0);
begin
l_ua <= unsigned (l_A);
l_bv0 <= (others => l_B(0));
l_bv1 <= (others => l_B(1));
l_bv2 <= (others => l_B(2));
l_bv3 <= (others => l_B(3));
l_bv4 <= (others => l_B(4));
l_bv5 <= (others => l_B(5));
l_bv6 <= (others => l_B(6));
l_bv7 <= (others => l_B(7));
l_p0 <= "0" & (l_bv0 and l_ua);
l_p1 <= ("0" & l_p0(WIDTH DOWNTO 1)) + ("0" & (l_bv1 and l_ua));
l_p2 <= ("0" & l_p1(WIDTH DOWNTO 1)) + ("0" & (l_bv2 and l_ua));
l_p3 <= ("0" & l_p2(WIDTH DOWNTO 1)) + ("0" & (l_bv3 and l_ua));
l_p4 <= ("0" & l_p3(WIDTH DOWNTO 1)) + ("0" & (l_bv4 and l_ua));
l_p5 <= ("0" & l_p4(WIDTH DOWNTO 1)) + ("0" & (l_bv5 and l_ua));
l_p6 <= ("0" & l_p5(WIDTH DOWNTO 1)) + ("0" & (l_bv6 and l_ua));
l_p7 <= ("0" & l_p6(WIDTH DOWNTO 1)) + ("0" & (l_bv7 and l_ua));
l_product <= l_p7 & l_p6(0) & l_p5(0)& l_p4(0)&
l_p3(0)& l_p2(0)& l_p1(0)& l_p0(0);
l_P <= std_logic_vector (l_product);

end Behavioral;

