----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:11:37 12/18/2017 
-- Design Name: 
-- Module Name:    l_M - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity l_M is
    Port ( L_A : in  STD_LOGIC_VECTOR (7 downto 0);
           L_B : in  STD_LOGIC_VECTOR (7 downto 0);
           L_P : out  STD_LOGIC_VECTOR (15 downto 0));
end l_M;

architecture Behavioral of l_M is
constant WIDTH: integer:=8;
signal l_ua, l_bv0, l_bv1, l_bv2, l_bv3, l_bv4, l_bv5, l_bv6, l_bv7 : unsigned (WIDTH - 1 downto 0);
signal l_bp, l_p0, l_p1, l_p2, l_p3,l_p4, l_p5,l_p6, l_p7 : unsigned (2*WIDTH - 1 downto 0);
begin
l_ua <= unsigned (l_A);
l_bv0 <= (others => l_B(0));
l_bv1 <= (others => l_B(1));
l_bv2 <= (others => l_B(2));
l_bv3 <= (others => l_B(3));
l_bv4 <= (others => l_B(4));
l_bv5 <= (others => l_B(5));
l_bv6 <= (others => l_B(6));
l_bv7 <= (others => l_B(7));
l_p0 <= "00000000" & (l_bv0 and l_ua);
l_p1 <= "0000000" & (l_bv1 and l_ua) & "0";
l_p2 <= "000000" & (l_bv2 and l_ua) & "00";
l_p3 <= "00000" & (l_bv3 and l_ua) & "000";
l_p4 <= "0000" & (l_bv4 and l_ua) & "0000";
l_p5 <= "000" & (l_bv5 and l_ua) & "00000";
l_p6 <= "00" & (l_bv6 and l_ua) & "000000";
l_p7 <= "0" & (l_bv7 and l_ua) & "0000000";
l_bp<=((l_p0+l_p1)+(l_p2+l_p3))+((l_p4+l_p5)+(l_p6+l_p7));
l_P<= std_logic_vector (l_bp);
end Behavioral;

