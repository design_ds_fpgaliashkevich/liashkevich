--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : VR_sch.vhf
-- /___/   /\     Timestamp : 11/30/2017 11:00:26
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family artix7 -flat -suppress -vhdl D:/XI_LABS/VR_lab1/VR_sch.vhf -w D:/XI_LABS/VR_lab1/VR_sch.sch
--Design Name: VR_sch
--Device: artix7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity VR_sch is
   port ( VR_a : in    std_logic; 
          VR_b : in    std_logic; 
          VR_c : in    std_logic; 
          VR_d : in    std_logic; 
          VR_e : in    std_logic; 
          VR_f : out   std_logic);
end VR_sch;

architecture BEHAVIORAL of VR_sch is
   attribute BOX_TYPE   : string ;
   signal XLXN_1 : std_logic;
   signal XLXN_2 : std_logic;
   signal XLXN_3 : std_logic;
   signal XLXN_4 : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>VR_c,
                I1=>XLXN_2,
                O=>XLXN_3);
   
   XLXI_2 : AND2
      port map (I0=>XLXN_4,
                I1=>XLXN_3,
                O=>VR_f);
   
   XLXI_3 : OR2
      port map (I0=>XLXN_1,
                I1=>VR_a,
                O=>XLXN_2);
   
   XLXI_4 : OR2
      port map (I0=>VR_e,
                I1=>VR_d,
                O=>XLXN_4);
   
   XLXI_5 : INV
      port map (I=>VR_b,
                O=>XLXN_1);
   
end BEHAVIORAL;


