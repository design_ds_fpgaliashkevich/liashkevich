----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:11:17 11/30/2017 
-- Design Name: 
-- Module Name:    VR_vhdl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VR_vhdl is
    Port ( VR_a : in  STD_LOGIC;
           VR_b : in  STD_LOGIC;
           VR_c : in  STD_LOGIC;
           VR_d : in  STD_LOGIC;
           VR_e : in  STD_LOGIC;
           VR_f : out  STD_LOGIC);
end VR_vhdl;

architecture Behavioral of VR_vhdl is

begin
	VR_f <= ((VR_a or (not VR_b)) and VR_c) and (VR_d or VR_e);

end Behavioral;

