/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *VL_P_2533777724;
char *IEEE_P_2592010699;
char *STD_STANDARD;
char *IEEE_P_1242562249;
char *IEEE_P_3499444699;
char *IEEE_P_3620187407;
char *UNISIM_P_0947159679;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000004134447467_2073120511_init();
    unisims_ver_m_00000000003927721830_1593237687_init();
    unisims_ver_m_00000000003317509437_1759035934_init();
    unisims_ver_m_00000000003405408344_3841093685_init();
    unisims_ver_m_00000000004221170615_0679316750_init();
    unisims_ver_m_00000000000236260522_2449448540_init();
    unisims_ver_m_00000000001508379050_3852734344_init();
    unisims_ver_m_00000000001773747898_2324208960_init();
    unisims_ver_m_00000000001773747898_2336946039_init();
    unisims_ver_m_00000000001773747898_3056262855_init();
    unisims_ver_m_00000000001108370118_0861236213_init();
    unisims_ver_m_00000000002601448656_3367321443_init();
    unisims_ver_m_00000000000909115699_2771340377_init();
    unisims_ver_m_00000000004098988994_0302322055_init();
    unisims_ver_m_00000000003848737514_1058825862_init();
    unisims_ver_m_00000000002768523630_0350466464_init();
    unisims_ver_m_00000000002768523630_2529709887_init();
    work_m_00000000000009106488_3591160656_init();
    work_m_00000000002975285465_1606142266_init();
    work_m_00000000000512988239_0817865115_init();
    work_m_00000000002975285465_3434776263_init();
    work_m_00000000000093673347_4036325881_init();
    work_m_00000000002364986475_2819328453_init();
    ieee_p_2592010699_init();
    ieee_p_1242562249_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    unisim_p_0947159679_init();
    vl_p_2533777724_init();
    work_a_4089179747_3212880686_init();
    work_a_1814251313_3212880686_init();
    work_a_0690566699_3212880686_init();


    xsi_register_tops("work_a_0690566699_3212880686");
    xsi_register_tops("work_m_00000000004134447467_2073120511");

    VL_P_2533777724 = xsi_get_engine_memory("vl_p_2533777724");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");

    return xsi_run_simulation(argc, argv);

}
