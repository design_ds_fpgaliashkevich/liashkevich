----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:56:16 12/20/2017 
-- Design Name: 
-- Module Name:    mM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mM is
    Port ( L_A : in  STD_LOGIC_VECTOR (7 downto 0);
           L_out1 : out  STD_LOGIC_VECTOR (15 downto 0));
end mM;

architecture Behavioral of mM is
signal l_prod,l_p6,l_p4,l_p3,l_p0:unsigned (15 downto 0);
begin
l_p0  <="00000000" & unsigned (l_A) & "";
l_p3  <="00000" & unsigned (l_A) & "000";
l_p4  <="0000" & unsigned (l_A) & "0000";
l_p6  <="00" & unsigned (l_A) & "000000";
 l_prod <= (l_p0 +l_p3 + l_p4 + l_p6);
l_out1 <= std_logic_vector (l_prod);
end Behavioral;

