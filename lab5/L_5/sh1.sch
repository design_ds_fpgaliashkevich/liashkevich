<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="L_A(7:0)" />
        <signal name="ce" />
        <signal name="clk" />
        <signal name="clr" />
        <signal name="fir(15:0)" />
        <signal name="rfd" />
        <signal name="rdy" />
        <signal name="ipfir(17:0)" />
        <port polarity="Input" name="L_A(7:0)" />
        <port polarity="Input" name="ce" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="clr" />
        <port polarity="Output" name="fir(15:0)" />
        <port polarity="Output" name="rfd" />
        <port polarity="Output" name="rdy" />
        <port polarity="Output" name="ipfir(17:0)" />
        <blockdef name="ip">
            <timestamp>2017-12-20T0:25:32</timestamp>
            <rect width="512" x="32" y="32" height="2016" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="1008" y2="1008" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
            <line x2="544" y1="1840" y2="1840" x1="576" />
            <line x2="544" y1="1872" y2="1872" x1="576" />
        </blockdef>
        <blockdef name="sh">
            <timestamp>2017-12-20T0:15:50</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <block symbolname="ip" name="XLXI_1">
            <blockpin signalname="L_A(7:0)" name="din(7:0)" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="ipfir(17:0)" name="dout(17:0)" />
            <blockpin signalname="rfd" name="rfd" />
            <blockpin signalname="rdy" name="rdy" />
        </block>
        <block symbolname="sh" name="XLXI_3">
            <blockpin signalname="L_A(7:0)" name="L_A(7:0)" />
            <blockpin signalname="clr" name="clr" />
            <blockpin signalname="ce" name="ce" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="fir(15:0)" name="F(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1536" y="256" name="XLXI_1" orien="R0">
        </instance>
        <branch name="L_A(7:0)">
            <wire x2="832" y1="336" y2="336" x1="672" />
            <wire x2="1536" y1="336" y2="336" x1="832" />
            <wire x2="832" y1="336" y2="1920" x1="832" />
            <wire x2="736" y1="1920" y2="2016" x1="736" />
            <wire x2="800" y1="2016" y2="2016" x1="736" />
            <wire x2="832" y1="1920" y2="1920" x1="736" />
        </branch>
        <iomarker fontsize="28" x="672" y="336" name="L_A(7:0)" orien="R180" />
        <branch name="ce">
            <wire x2="800" y1="2144" y2="2144" x1="368" />
        </branch>
        <iomarker fontsize="28" x="368" y="2144" name="ce" orien="R180" />
        <branch name="clk">
            <wire x2="800" y1="2208" y2="2208" x1="368" />
            <wire x2="368" y1="2208" y2="2224" x1="368" />
            <wire x2="576" y1="2224" y2="2224" x1="368" />
            <wire x2="576" y1="1264" y2="1264" x1="400" />
            <wire x2="1536" y1="1264" y2="1264" x1="576" />
            <wire x2="576" y1="1264" y2="2224" x1="576" />
        </branch>
        <iomarker fontsize="28" x="400" y="1264" name="clk" orien="R180" />
        <branch name="clr">
            <wire x2="800" y1="2080" y2="2080" x1="464" />
        </branch>
        <iomarker fontsize="28" x="464" y="2080" name="clr" orien="R180" />
        <branch name="fir(15:0)">
            <wire x2="1312" y1="2016" y2="2016" x1="1184" />
            <wire x2="1312" y1="2016" y2="2224" x1="1312" />
        </branch>
        <iomarker fontsize="28" x="1312" y="2224" name="fir(15:0)" orien="R90" />
        <branch name="rfd">
            <wire x2="2240" y1="2096" y2="2096" x1="2112" />
        </branch>
        <branch name="rdy">
            <wire x2="2240" y1="2128" y2="2128" x1="2112" />
        </branch>
        <iomarker fontsize="28" x="2240" y="2096" name="rfd" orien="R0" />
        <iomarker fontsize="28" x="2240" y="2128" name="rdy" orien="R0" />
        <branch name="ipfir(17:0)">
            <wire x2="2272" y1="336" y2="336" x1="2112" />
        </branch>
        <iomarker fontsize="28" x="2272" y="336" name="ipfir(17:0)" orien="R0" />
        <instance x="800" y="2240" name="XLXI_3" orien="R0">
        </instance>
    </sheet>
</drawing>