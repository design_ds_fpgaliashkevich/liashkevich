<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="a(7:0)" />
        <signal name="d(7:0)" />
        <signal name="c(7:0)" />
        <signal name="XLXN_133(7:0)" />
        <signal name="XLXN_200(7:0)" />
        <signal name="XLXN_201(7:0)" />
        <signal name="XLXN_203" />
        <signal name="res2(15:0)" />
        <signal name="res1(7:0)" />
        <signal name="XLXN_209(7:0)" />
        <signal name="XLXN_210(15:0)" />
        <signal name="XLXN_211(15:0)" />
        <signal name="XLXN_212(15:0)" />
        <signal name="res3(15:0)" />
        <signal name="ZERO" />
        <signal name="ONE" />
        <signal name="XLXN_219(15:0)" />
        <signal name="XLXN_220(15:0)" />
        <signal name="XLXN_221(15:0)" />
        <signal name="res(15:0)" />
        <signal name="ce" />
        <signal name="clk" />
        <signal name="clr" />
        <signal name="XLXN_226" />
        <signal name="XLXN_228" />
        <signal name="XLXN_229" />
        <signal name="XLXN_230" />
        <signal name="XLXN_231" />
        <signal name="XLXN_232" />
        <signal name="XLXN_233" />
        <signal name="XLXN_234" />
        <signal name="XLXN_235" />
        <signal name="XLXN_236" />
        <signal name="XLXN_237" />
        <signal name="XLXN_238" />
        <signal name="XLXN_239(15:0)" />
        <signal name="XLXN_240(15:0)" />
        <port polarity="Input" name="a(7:0)" />
        <port polarity="Input" name="d(7:0)" />
        <port polarity="Input" name="c(7:0)" />
        <port polarity="Output" name="res2(15:0)" />
        <port polarity="Output" name="res1(7:0)" />
        <port polarity="Output" name="res3(15:0)" />
        <port polarity="Input" name="ZERO" />
        <port polarity="Input" name="ONE" />
        <port polarity="Output" name="res(15:0)" />
        <port polarity="Input" name="ce" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="clr" />
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="conv">
            <timestamp>2017-12-24T19:23:57</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="adsu16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="128" />
            <line x2="128" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="mul2">
            <timestamp>2017-12-24T19:29:8</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="mul3">
            <timestamp>2017-12-25T20:31:33</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="add8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <rect width="64" x="0" y="-332" height="24" />
            <rect width="64" x="0" y="-204" height="24" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-128" y2="-128" x1="448" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <block symbolname="fd8ce" name="XLXI_1">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="a(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_200(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_2">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="d(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_201(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_46">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="c(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_133(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="mul2" name="XLXI_65">
            <blockpin signalname="XLXN_133(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_133(7:0)" name="b(7:0)" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="res2(15:0)" name="p(15:0)" />
        </block>
        <block symbolname="conv" name="XLXI_72">
            <blockpin signalname="XLXN_209(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_210(15:0)" name="s16(15:0)" />
        </block>
        <block symbolname="mul3" name="XLXI_75">
            <blockpin signalname="XLXN_212(15:0)" name="a(15:0)" />
            <blockpin signalname="XLXN_239(15:0)" name="b(15:0)" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="res3(15:0)" name="p(15:0)" />
        </block>
        <block symbolname="add8" name="XLXI_85">
            <blockpin signalname="XLXN_200(7:0)" name="A(7:0)" />
            <blockpin signalname="XLXN_201(7:0)" name="B(7:0)" />
            <blockpin signalname="XLXN_203" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="res1(7:0)" name="S(7:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_86">
            <blockpin signalname="XLXN_203" name="G" />
        </block>
        <block symbolname="fd16ce" name="XLXI_89">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="res2(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_211(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_90">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="res1(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_209(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_91">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="XLXN_211(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_212(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_92">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="XLXN_210(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_240(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_93">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="res3(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_220(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="adsu16" name="XLXI_58">
            <blockpin signalname="XLXN_219(15:0)" name="A(15:0)" />
            <blockpin signalname="ZERO" name="ADD" />
            <blockpin signalname="XLXN_220(15:0)" name="B(15:0)" />
            <blockpin signalname="ONE" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_221(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="constant" name="XLXI_96">
            <attr value="7" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_219(15:0)" name="O" />
        </block>
        <block symbolname="fd16ce" name="XLXI_97">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="XLXN_221(15:0)" name="D(15:0)" />
            <blockpin signalname="res(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_98">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="XLXN_240(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_239(15:0)" name="Q(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="1008" y="1024" name="XLXI_1" orien="R0" />
        <instance x="1008" y="1712" name="XLXI_2" orien="R0" />
        <branch name="a(7:0)">
            <wire x2="1008" y1="768" y2="768" x1="752" />
        </branch>
        <branch name="d(7:0)">
            <wire x2="1008" y1="1456" y2="1456" x1="720" />
        </branch>
        <iomarker fontsize="28" x="752" y="768" name="a(7:0)" orien="R180" />
        <iomarker fontsize="28" x="720" y="1456" name="d(7:0)" orien="R180" />
        <instance x="1008" y="2448" name="XLXI_46" orien="R0" />
        <branch name="c(7:0)">
            <wire x2="1008" y1="2192" y2="2192" x1="704" />
        </branch>
        <iomarker fontsize="28" x="704" y="2192" name="c(7:0)" orien="R180" />
        <instance x="1840" y="2096" name="XLXI_65" orien="R0">
        </instance>
        <branch name="XLXN_133(7:0)">
            <wire x2="1616" y1="2192" y2="2192" x1="1392" />
            <wire x2="1616" y1="2192" y2="2240" x1="1616" />
            <wire x2="1840" y1="2240" y2="2240" x1="1616" />
            <wire x2="1616" y1="2176" y2="2192" x1="1616" />
            <wire x2="1840" y1="2176" y2="2176" x1="1616" />
        </branch>
        <instance x="1584" y="1360" name="XLXI_85" orien="R0" />
        <branch name="XLXN_200(7:0)">
            <wire x2="1488" y1="768" y2="768" x1="1392" />
            <wire x2="1488" y1="768" y2="1040" x1="1488" />
            <wire x2="1584" y1="1040" y2="1040" x1="1488" />
        </branch>
        <branch name="XLXN_201(7:0)">
            <wire x2="1488" y1="1456" y2="1456" x1="1392" />
            <wire x2="1488" y1="1168" y2="1456" x1="1488" />
            <wire x2="1584" y1="1168" y2="1168" x1="1488" />
        </branch>
        <instance x="1568" y="880" name="XLXI_86" orien="R0" />
        <branch name="XLXN_203">
            <wire x2="1584" y1="736" y2="912" x1="1584" />
            <wire x2="1632" y1="736" y2="736" x1="1584" />
            <wire x2="1632" y1="736" y2="752" x1="1632" />
        </branch>
        <instance x="2688" y="2432" name="XLXI_89" orien="R0" />
        <branch name="res2(15:0)">
            <wire x2="2608" y1="2176" y2="2176" x1="2416" />
            <wire x2="2688" y1="2176" y2="2176" x1="2608" />
            <wire x2="2608" y1="2080" y2="2176" x1="2608" />
        </branch>
        <instance x="2304" y="1344" name="XLXI_90" orien="R0" />
        <branch name="res1(7:0)">
            <wire x2="2160" y1="1104" y2="1104" x1="2032" />
            <wire x2="2160" y1="1088" y2="1104" x1="2160" />
            <wire x2="2192" y1="1088" y2="1088" x1="2160" />
            <wire x2="2304" y1="1088" y2="1088" x1="2192" />
            <wire x2="2192" y1="944" y2="944" x1="2176" />
            <wire x2="2192" y1="944" y2="1088" x1="2192" />
        </branch>
        <instance x="2928" y="1120" name="XLXI_72" orien="R0">
        </instance>
        <branch name="XLXN_209(7:0)">
            <wire x2="2928" y1="1088" y2="1088" x1="2688" />
        </branch>
        <instance x="3296" y="2432" name="XLXI_91" orien="R0" />
        <instance x="3472" y="1344" name="XLXI_92" orien="R0" />
        <branch name="XLXN_210(15:0)">
            <wire x2="3472" y1="1088" y2="1088" x1="3312" />
        </branch>
        <branch name="XLXN_211(15:0)">
            <wire x2="3296" y1="2176" y2="2176" x1="3072" />
        </branch>
        <instance x="4096" y="1536" name="XLXI_75" orien="R0">
        </instance>
        <branch name="XLXN_212(15:0)">
            <wire x2="3936" y1="2176" y2="2176" x1="3680" />
            <wire x2="4096" y1="1616" y2="1616" x1="3936" />
            <wire x2="3936" y1="1616" y2="2176" x1="3936" />
        </branch>
        <instance x="5024" y="1888" name="XLXI_93" orien="R0" />
        <branch name="res3(15:0)">
            <wire x2="4848" y1="1616" y2="1616" x1="4672" />
            <wire x2="4848" y1="1616" y2="1632" x1="4848" />
            <wire x2="4944" y1="1632" y2="1632" x1="4848" />
            <wire x2="5024" y1="1632" y2="1632" x1="4944" />
            <wire x2="4944" y1="1440" y2="1632" x1="4944" />
        </branch>
        <branch name="ZERO">
            <wire x2="5712" y1="1744" y2="1792" x1="5712" />
            <wire x2="5792" y1="1744" y2="1744" x1="5712" />
            <wire x2="5792" y1="1744" y2="1920" x1="5792" />
            <wire x2="5888" y1="1920" y2="1920" x1="5792" />
            <wire x2="5888" y1="1904" y2="1920" x1="5888" />
        </branch>
        <branch name="ONE">
            <wire x2="5888" y1="1424" y2="1520" x1="5888" />
        </branch>
        <instance x="5888" y="1968" name="XLXI_58" orien="R0" />
        <iomarker fontsize="28" x="5888" y="1424" name="ONE" orien="R270" />
        <iomarker fontsize="28" x="5712" y="1792" name="ZERO" orien="R90" />
        <instance x="5456" y="1376" name="XLXI_96" orien="R0">
        </instance>
        <branch name="XLXN_219(15:0)">
            <wire x2="5744" y1="1408" y2="1408" x1="5600" />
            <wire x2="5744" y1="1408" y2="1648" x1="5744" />
            <wire x2="5888" y1="1648" y2="1648" x1="5744" />
        </branch>
        <branch name="XLXN_220(15:0)">
            <wire x2="5648" y1="1632" y2="1632" x1="5408" />
            <wire x2="5648" y1="1632" y2="1776" x1="5648" />
            <wire x2="5888" y1="1776" y2="1776" x1="5648" />
        </branch>
        <instance x="5792" y="2688" name="XLXI_97" orien="R0" />
        <branch name="XLXN_221(15:0)">
            <wire x2="5792" y1="2432" y2="2432" x1="5728" />
            <wire x2="5728" y1="2432" y2="2720" x1="5728" />
            <wire x2="6416" y1="2720" y2="2720" x1="5728" />
            <wire x2="6416" y1="1712" y2="1712" x1="6336" />
            <wire x2="6416" y1="1712" y2="2720" x1="6416" />
        </branch>
        <branch name="res(15:0)">
            <wire x2="6528" y1="2432" y2="2432" x1="6176" />
            <wire x2="6528" y1="2432" y2="2448" x1="6528" />
        </branch>
        <iomarker fontsize="28" x="6528" y="2448" name="res(15:0)" orien="R90" />
        <branch name="ce">
            <wire x2="816" y1="832" y2="832" x1="736" />
            <wire x2="1008" y1="832" y2="832" x1="816" />
            <wire x2="816" y1="832" y2="1520" x1="816" />
            <wire x2="1008" y1="1520" y2="1520" x1="816" />
            <wire x2="816" y1="1520" y2="1744" x1="816" />
            <wire x2="816" y1="1744" y2="2256" x1="816" />
            <wire x2="1008" y1="2256" y2="2256" x1="816" />
            <wire x2="2096" y1="1744" y2="1744" x1="816" />
            <wire x2="2432" y1="1744" y2="1744" x1="2096" />
            <wire x2="2432" y1="1744" y2="2240" x1="2432" />
            <wire x2="2688" y1="2240" y2="2240" x1="2432" />
            <wire x2="2944" y1="1744" y2="1744" x1="2432" />
            <wire x2="3120" y1="1744" y2="1744" x1="2944" />
            <wire x2="3120" y1="1744" y2="2240" x1="3120" />
            <wire x2="3296" y1="2240" y2="2240" x1="3120" />
            <wire x2="3120" y1="2240" y2="2464" x1="3120" />
            <wire x2="4752" y1="2464" y2="2464" x1="3120" />
            <wire x2="5264" y1="2464" y2="2464" x1="4752" />
            <wire x2="5264" y1="2464" y2="2496" x1="5264" />
            <wire x2="5792" y1="2496" y2="2496" x1="5264" />
            <wire x2="2096" y1="1152" y2="1744" x1="2096" />
            <wire x2="2304" y1="1152" y2="1152" x1="2096" />
            <wire x2="3472" y1="1152" y2="1152" x1="2944" />
            <wire x2="2944" y1="1152" y2="1408" x1="2944" />
            <wire x2="2944" y1="1408" y2="1744" x1="2944" />
            <wire x2="3920" y1="1408" y2="1408" x1="2944" />
            <wire x2="3920" y1="1152" y2="1408" x1="3920" />
            <wire x2="4112" y1="1152" y2="1152" x1="3920" />
            <wire x2="4752" y1="1696" y2="2464" x1="4752" />
            <wire x2="5024" y1="1696" y2="1696" x1="4752" />
        </branch>
        <branch name="clr">
            <wire x2="928" y1="992" y2="992" x1="768" />
            <wire x2="1008" y1="992" y2="992" x1="928" />
            <wire x2="928" y1="992" y2="1680" x1="928" />
            <wire x2="1008" y1="1680" y2="1680" x1="928" />
            <wire x2="928" y1="1680" y2="1856" x1="928" />
            <wire x2="928" y1="1856" y2="2416" x1="928" />
            <wire x2="1008" y1="2416" y2="2416" x1="928" />
            <wire x2="2304" y1="1856" y2="1856" x1="928" />
            <wire x2="2496" y1="1856" y2="1856" x1="2304" />
            <wire x2="2496" y1="1856" y2="2400" x1="2496" />
            <wire x2="2688" y1="2400" y2="2400" x1="2496" />
            <wire x2="3472" y1="1856" y2="1856" x1="2496" />
            <wire x2="3472" y1="1856" y2="2048" x1="3472" />
            <wire x2="2304" y1="1312" y2="1856" x1="2304" />
            <wire x2="3216" y1="2048" y2="2400" x1="3216" />
            <wire x2="3296" y1="2400" y2="2400" x1="3216" />
            <wire x2="3216" y1="2400" y2="2496" x1="3216" />
            <wire x2="5024" y1="2496" y2="2496" x1="3216" />
            <wire x2="5024" y1="2496" y2="2656" x1="5024" />
            <wire x2="5792" y1="2656" y2="2656" x1="5024" />
            <wire x2="3472" y1="2048" y2="2048" x1="3216" />
            <wire x2="3456" y1="1488" y2="1584" x1="3456" />
            <wire x2="3472" y1="1584" y2="1584" x1="3456" />
            <wire x2="3472" y1="1584" y2="1856" x1="3472" />
            <wire x2="4112" y1="1488" y2="1488" x1="3456" />
            <wire x2="3472" y1="1312" y2="1584" x1="3472" />
            <wire x2="4112" y1="1312" y2="1488" x1="4112" />
            <wire x2="5024" y1="1856" y2="2496" x1="5024" />
        </branch>
        <iomarker fontsize="28" x="736" y="832" name="ce" orien="R180" />
        <iomarker fontsize="28" x="752" y="896" name="clk" orien="R180" />
        <iomarker fontsize="28" x="768" y="992" name="clr" orien="R180" />
        <branch name="clk">
            <wire x2="864" y1="896" y2="896" x1="752" />
            <wire x2="880" y1="896" y2="896" x1="864" />
            <wire x2="1008" y1="896" y2="896" x1="880" />
            <wire x2="880" y1="896" y2="1584" x1="880" />
            <wire x2="1008" y1="1584" y2="1584" x1="880" />
            <wire x2="880" y1="1584" y2="1792" x1="880" />
            <wire x2="880" y1="1792" y2="2320" x1="880" />
            <wire x2="1008" y1="2320" y2="2320" x1="880" />
            <wire x2="2080" y1="1792" y2="1792" x1="880" />
            <wire x2="2448" y1="1792" y2="1792" x1="2080" />
            <wire x2="2448" y1="1792" y2="2304" x1="2448" />
            <wire x2="2688" y1="2304" y2="2304" x1="2448" />
            <wire x2="2960" y1="1792" y2="1792" x1="2448" />
            <wire x2="3104" y1="1792" y2="1792" x1="2960" />
            <wire x2="3104" y1="1792" y2="2304" x1="3104" />
            <wire x2="3296" y1="2304" y2="2304" x1="3104" />
            <wire x2="3104" y1="2304" y2="2416" x1="3104" />
            <wire x2="4736" y1="2416" y2="2416" x1="3104" />
            <wire x2="4736" y1="2416" y2="2560" x1="4736" />
            <wire x2="5792" y1="2560" y2="2560" x1="4736" />
            <wire x2="864" y1="2048" y2="2320" x1="864" />
            <wire x2="880" y1="2320" y2="2320" x1="864" />
            <wire x2="1456" y1="2048" y2="2048" x1="864" />
            <wire x2="1456" y1="2048" y2="2336" x1="1456" />
            <wire x2="1840" y1="2336" y2="2336" x1="1456" />
            <wire x2="2080" y1="1216" y2="1792" x1="2080" />
            <wire x2="2304" y1="1216" y2="1216" x1="2080" />
            <wire x2="3472" y1="1216" y2="1216" x1="2960" />
            <wire x2="2960" y1="1216" y2="1504" x1="2960" />
            <wire x2="2960" y1="1504" y2="1792" x1="2960" />
            <wire x2="3904" y1="1504" y2="1504" x1="2960" />
            <wire x2="4096" y1="1776" y2="1776" x1="3104" />
            <wire x2="3104" y1="1776" y2="1792" x1="3104" />
            <wire x2="3904" y1="1216" y2="1504" x1="3904" />
            <wire x2="4112" y1="1216" y2="1216" x1="3904" />
            <wire x2="4736" y1="1760" y2="2416" x1="4736" />
            <wire x2="5024" y1="1760" y2="1760" x1="4736" />
        </branch>
        <iomarker fontsize="28" x="2176" y="944" name="res1(7:0)" orien="R180" />
        <iomarker fontsize="28" x="2608" y="2080" name="res2(15:0)" orien="R270" />
        <iomarker fontsize="28" x="4944" y="1440" name="res3(15:0)" orien="R270" />
        <instance x="4112" y="1344" name="XLXI_98" orien="R0" />
        <branch name="XLXN_239(15:0)">
            <wire x2="4016" y1="1520" y2="1680" x1="4016" />
            <wire x2="4096" y1="1680" y2="1680" x1="4016" />
            <wire x2="4576" y1="1520" y2="1520" x1="4016" />
            <wire x2="4576" y1="1088" y2="1088" x1="4496" />
            <wire x2="4576" y1="1088" y2="1520" x1="4576" />
        </branch>
        <branch name="XLXN_240(15:0)">
            <wire x2="4112" y1="1088" y2="1088" x1="3856" />
        </branch>
    </sheet>
</drawing>